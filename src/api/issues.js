import constants from '../constants'
import httpRequest from '../services/http'

export default {
  fetchIssues () {
    const api = true
    const method = constants.ISSUES_ALL_METHOD
    const path = constants.ISSUES_ALL_PATH
    const data = {} // required by Axios
    const opts = {
      api,
      data,
      method,
      path
    }

    return httpRequest(opts)
  }
}
