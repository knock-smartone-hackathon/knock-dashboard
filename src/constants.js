// const apiPath = '/api'

module.exports = {
  // Endpoints
  ISSUES_ALL_METHOD: 'GET',
  // ISSUES_ALL_PATH: 'http://knock-rest.azurewebsites.net/cases',
  ISSUES_ALL_PATH: 'http://5801902c.ngrok.io/cases',

  ALLOWED_HTTP_METHODS: ['get', 'post', 'put', 'delete']
}
