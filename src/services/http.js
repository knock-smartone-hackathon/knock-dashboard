import axios from 'axios'
import includes from 'lodash/includes'
import CONSTANTS from '../constants'

const baseApiURL = ''

function getHeaders () {
  return {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
}

/**
 * Http request service.
 *
 * options = {
 *   api,     // API request
 *   method,  // 'get', 'post', 'put' or 'delete'
 *   path,
 *   data,
 *   headers,
 * }
 *
 * @param {object} options - Request options
 */
export default function httpRequest (options) {
  const method = includes(CONSTANTS.ALLOWED_HTTP_METHODS, options.method)
    ? options.method
    : 'get'
  const url = options.api ? baseApiURL + options.path : options.path
  const data = options.data
  const headers = getHeaders()
  const opts = {
    method,
    url,
    data,
    headers
  }
  if (options.headers) opts.headers = options.headers

  return axios(opts)
}
