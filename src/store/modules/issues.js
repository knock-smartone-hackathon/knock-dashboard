import { issues as types } from '../mutation-types'
import api from '../../api/issues'

function progress (value) {
  switch (value) {
    case 'opened':
      return 10
    case 'notified_technician':
      return 33
    case 'notified_customer':
      return 75
    case 'closed':
      return 100
  }
}

function modelIssue (data) {
  return {
    id: data.id,
    avatar: { url: 'static/img/avatars/avatar-1.jpg', status: 'success' },
    user: { name: data.user.name },
    description: `${data.type} - ${data.object}`,
    progress: progress(data.workflow_status),
    activity: data.updated_at
  }
}

// initial state
const initialState = {
  issues: [
    {
      avatar: { url: 'static/img/avatars/avatar-1.jpg', status: 'success' },
      user: { name: 'Mr. Chan Tai Man', location: 'PARK YOHO Genova, BLK 23, 69F' },
      description: 'Burnt lightbulb on 21/F',
      progress: progress('notified_technician'),
      activity: '10 sec ago'
    },
    {
      avatar: { url: 'static/img/avatars/avatar-2.jpg', status: 'sucess' },
      user: { name: 'Ms. Ma Ma Day', location: 'PARK YOHO Sicilia, BLK 27, 1C' },
      description: 'Lightbulb in Play Room 2 needs replacing',
      progress: progress('notified_customer'),
      activity: '5 minutes ago'
    },
    {
      avatar: { url: 'static/img/avatars/avatar-2.jpg', status: 'success' },
      user: { name: 'Mrs. Paula Wong', location: 'Park Vista 1A, BLK 39, 56F' },
      description: 'Dirty floors in common area on 27/F',
      progress: progress('closed'),
      activity: '5 minutes ago'
    },
    {
      avatar: { url: 'static/img/avatars/avatar-3.jpg', status: 'danger' },
      user: { name: 'Mr. CHAN, Michael', location: 'Park Vista 1A, BLK 39, 56F' },
      description: 'Dirty floors in common area on 27/F',
      progress: progress('opened'),
      activity: '5 minutes ago'
    },
    {
      avatar: { url: 'static/img/avatars/avatar-2.jpg', status: 'success' },
      user: { name: 'Mr. CHOW, Yun-Fat', location: 'Eight Regency, BLK 3, 26F' },
      description: 'Dirty floors in common area on 27/F',
      progress: progress('notified_customer'),
      activity: '5 minutes ago'
    },
    {
      avatar: { url: 'static/img/avatars/avatar-1.jpg', status: 'success' },
      user: { name: 'Mrs. LI Kar Shing', location: 'Cullinan West, BLK 27, 167F' },
      description: 'Dirty floors in common area on 27/F',
      progress: progress('closed'),
      activity: '5 minutes ago'
    },
    {
      avatar: { url: 'static/img/avatars/avatar-4.png', status: 'success' },
      user: { name: 'Ms. LAM Carrie', location: 'Park Vista 1A, BLK 3, 16F' },
      description: 'Dirty floors in common area on 27/F',
      progress: progress('closed'),
      activity: '5 minutes ago'
    },
    {
      avatar: { url: 'static/img/avatars/avatar-4.png', status: 'success' },
      user: { name: 'Mr. SUN Yat Sen', location: 'Park Vista 1A, BLK 39, 56F' },
      description: 'Dirty floors in common area on 27/F',
      progress: progress('notified_customer'),
      activity: '5 minutes ago'
    },
    {
      avatar: { url: 'static/img/avatars/avatar-1.jpg', status: 'success' },
      user: { name: 'Mrs. MA Jack', location: 'Park Vista 1A, BLK 39, 56F' },
      description: 'Dirty floors in common area on 27/F',
      progress: progress('notified_technician'),
      activity: '5 minutes ago'
    }
  ]
}

// getters
const getters = {
  getIssues: state => state.issues
}

// actions
const actions = {
  fetchIssues ({ commit }) {
    return api.fetchIssues().then((res) => {
      const issues = []
      if (res.data) {
        res.data.map((item) => {
          issues.push(modelIssue(item))
        })
        return new Promise((resolve) => {
          resolve(issues)
        })
      }
    }).catch((err) => {
      console.log(err)
    })
    // commit(types.ADD_ISSUES)
  }
}

// mutations
/* eslint-disable no-param-reassign */
const mutations = {
  [types.ADD_ISSUES] (state) {
  }
}
/* eslint-enable no-param-reassign */

export default {
  state: initialState,
  getters,
  actions,
  mutations
}
